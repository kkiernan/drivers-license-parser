<?php

namespace Kiernan;

use Exception;
use InvalidArgumentException;

class LicenseFactory
{
	/**
	 * Version codes for the AAMVA standards.
	 * 
	 * @var array
	 */
	protected static $aamvaVersions = [
		'01' => '2000',
		'02' => '2003',
		'03' => '2005',
		'04' => '2009',
		'05' => '2010',
		'06' => '2011',
		'07' => '2012',
		'08' => '2013',
	];

	/**
	 * Create a new license instance.
	 * 
	 * @param string $data The raw license data.
	 *
	 * @return License
	 */
	public static function create($data)
	{
		// Make sure we have a string to work with.
		if ( ! is_string($data))
		{
			throw new InvalidArgumentException("Argument must be of type string");
		}

		// Check for compliance indicator present in AAMVA standard.
		if (substr($data, 0, 1) === '@')
		{
			return self::createFromAAMVAStandard($data);
		}

		// Addtional format checks...
	}

	/**
	 * Create a license from a specific version of the AAMVA standard.
	 * 
	 * @param string $data The raw license data.
	 *
	 * @return Kiernan\Formats\AAMVA      
	 */
	protected static function createFromAAMVAStandard($data)
	{
		// Get the version number located at the 15th position
		// of the string as defined by the standard.
		$version = self::$aamvaVersions[substr($data, 15, 2)];

		// Set up the class name.
		$namespace = "\Kiernan\Formats\\";
		$class = "AAMVA$version";
		$fullyQualifiedName = $namespace.$class;

		// Throw an exception if the specific AAMVA class does not exist. 
		if ( ! class_exists($fullyQualifiedName))
		{
			throw new Exception("Could not create license from [$class] format");
		}

		// Return a new instance of the AAMVA implemenation.
		return new $fullyQualifiedName($data);
	}

}

