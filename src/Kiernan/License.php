<?php

namespace Kiernan;

interface License
{
	public function get($property);
}