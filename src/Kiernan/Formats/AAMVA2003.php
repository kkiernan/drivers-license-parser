<?php

namespace Kiernan\Formats;

class AAMVA2003 extends AAMVA
{
	const GIVEN_NAMES = '/DCT(\S*)/';
	const NAME_SUFFIX = '/DCU(\S*)/';
	const WEIGHT_RANGE = '/DCE(\d)/';

	protected $givenNames;
	protected $nameSuffix;
	protected $weightRange;

	/**
	 * Create a new instance of the class. Add any additional properties
	 * to this implementation that do not exist in the abstract class.
	 * 
	 * @param string $data
	 */
	public function __construct($data)
	{
		parent::__construct($data);

		$this->givenNames = $this->find($data, self::GIVEN_NAMES);
		$this->nameSuffix = $this->find($data, self::NAME_SUFFIX);
		$this->weightRange = $this->find($data, self::WEIGHT_RANGE);
	}
}