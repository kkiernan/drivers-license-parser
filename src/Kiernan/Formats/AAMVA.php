<?php

namespace Kiernan\Formats;

use InvalidArgumentException;
use Kiernan\License;

abstract class AAMVA implements License
{
	/*
	 * Provide more readable names for regex patterns.
	 */
	
	const VEHICLE_CLASS = '/DCA(\S*)/';
	const RESTRICTION_CODES = '/DCB(\S*)/';
	const ENDORSEMENT_CODES = '/DCD(\S*)/';
	const EXPIRATION = '/DBA(\S*)/';
	const FAMILY_NAME = '/DCS(\S*)/';
	const ISSUE_DATE = '/DBD(\S*)/';
	const DATE_OF_BIRTH = '/DBB(\S*)/';
	const GENDER = '/DBC(\S*)/';
	const EYE_COLOR = '/DAY(\S*)/';
	const HEIGHT = '/DAU(\S* [in|cm]{2,2})/';
	const STREET_ADDRESS = '/DAG(.*)/';
	const CITY = '/DAI(\S*)/';
	const STATE = '/DAJ(\S*)/';
	const ZIP = '/DAK(\d*)/';
	const CUSTOMER_ID = '/DAQ(\S*)/';
	const DOCUMENT_DISCRIMINATOR = '/DCF(\d*)/';
	const COUNTRY_ID = '/DCG(\S*)/';
	const VEHICLE_CODES = '/DCH(\S*)/';

	/**
	 * Substitute this text for items that are not found.
	 */
	const NOT_AVAILABLE = 'N/A';

	/*
	 * ----------------------------------------------------------------------
	 *  ANSI D20 Codes and Abbreviations
	 *  ----------------------------------------------------------------------
	 */

	protected static $genders = [
		0 => 'Unknown',
		1 => 'Male',
		2 => 'Female',
		9 => 'Not Specified',
	];

	protected static $abbreviations = [
		'BAL' => 'Bald',
		'BLK' => 'Black',
		'BLN' => 'Blond',
		'BLU' => 'Blue',
		'BRO' => 'Brown',
		'DIC' => 'Dichromatic',
		'GRY' => 'Gray',
		'GRN' => 'Green',
		'HAZ' => 'Hazel',
		'MAR' => 'Maroon',
		'PNK' => 'Pink',
		'RED' => 'Red/Auburn',
		'SDY' => 'Sandy',
		'WHI' => 'White',
		'UNK' => 'Unknown',
	];

	protected static $weightRanges = [
		0 => '0 - 70 lbs',
		1 => '71 - 100 lbs',
		2 => '101 - 130 lbs',
		3 => '131 - 160 lbs',
		4 => '161 - 190 lbs',
		5 => '191 - 220 lbs',
		6 => '221 - 250 lbs',
		7 => '251 - 280 lbs',
		8 => '281 - 320 lbs',
		9 => '321+ lbs',
	];

	/*
	 * ----------------------------------------------------------------------
	 *  Header Data
	 *  ----------------------------------------------------------------------
	 */
	
	protected $complianceIndicator;
	protected $dataSeparator;
	protected $recordSeparator;
	protected $segmentTerminator;
	protected $fileType;
	protected $issuerId;
	protected $aamvaVersion;
	protected $jurisdictionVersion;
	protected $numberOfEntries;

	/*
	 * ----------------------------------------------------------------------
	 *  Subfile Designator
	 *  ----------------------------------------------------------------------
	 */

	protected $subfileType;
	protected $offset;
	protected $length;
	
	/*
	 * ----------------------------------------------------------------------
	 *  Mandatory Data Elements
	 *  ----------------------------------------------------------------------
	 */
	
	protected $vehicleClass;
	protected $restrictionCodes;
	protected $endorsementCodes;
	protected $expiration;
	protected $familyName;
	protected $issueDate;
	protected $dateOfBirth;
	protected $gender;
	protected $eyeColor;
	protected $height;
	protected $streetAddress;
	protected $city;
	protected $state;
	protected $zip;
	protected $customerNumber;
	protected $documentDiscriminator;
	protected $countryId;
	protected $vehicleCode;

	/*
	 * ----------------------------------------------------------------------
	 *  Public Methods
	 *  ----------------------------------------------------------------------
	 */

	/**
	 * Create a new instance of an AAMVA subclass.
	 * 
	 * @param string $data
	 */
	public function __construct($data)
	{
		if ( ! is_string($data))
		{
			throw new InvalidArgumentException("Argument must be of type string");
		}

		$this->complianceIndicator = substr($data, 0, 1);
		$this->dataSeparator = substr($data, 1, 1);
		$this->recordSeparator = substr($data, 2, 1);
		$this->segmentTerminator = substr($data, 3, 1);
		$this->fileType = substr($data, 4, 5);
		$this->issuerId = substr($data, 9, 6);
		$this->aamvaVersion = substr($data, 15, 2);
		$this->jurisdictionVersion = substr($data, 17, 2);
		$this->numberOfEntries = substr($data, 19, 2);
		$this->subfileType = substr($data, 21, 2);
		$this->offset = substr($data, 23, 4);
		$this->length = substr($data, 27, 4);
		$this->vehicleClass = $this->find($data, self::VEHICLE_CLASS);
		$this->restrictionCodes = $this->find($data, self::RESTRICTION_CODES);
		$this->endorsementCodes = $this->find($data, self::ENDORSEMENT_CODES);
		$this->expiration = $this->find($data, self::EXPIRATION);
		$this->familyName = $this->find($data, self::FAMILY_NAME);
		$this->issueDate = $this->find($data, self::ISSUE_DATE);
		$this->dateOfBirth = $this->find($data, self::DATE_OF_BIRTH);
		$this->gender = $this->find($data, self::GENDER);
		$this->eyeColor = $this->find($data, self::EYE_COLOR);
		$this->height = $this->find($data, self::HEIGHT);
		$this->streetAddress = $this->find($data, self::STREET_ADDRESS);
		$this->city = $this->find($data, self::CITY);
		$this->state = $this->find($data, self::STATE);
		$this->zip = $this->find($data, self::ZIP);
		$this->customerId = $this->find($data, self::CUSTOMER_ID);
		$this->documentDiscriminator = $this->find($data, self::DOCUMENT_DISCRIMINATOR);
		$this->countryId = $this->find($data, self::COUNTRY_ID);
		$this->vehicleCodes = $this->find($data, self::VEHICLE_CODES);
	}

	/**
	 * Generic getter for protected properties.
	 * 
	 * @param  string $property
	 * @return mixed
	 */
	public function get($property)
	{
		if ( ! property_exists($this, $property))
		{
			throw new InvalidArgumentException("Property $property does not exist");
		}

		if ($property === 'gender')
		{
			return $this->getGender();
		}

		if ($property === 'eyeColor')
		{
			return $this->getEyeColor();
		}

		return $this->$property;
	}

	/*
	 * ----------------------------------------------------------------------
	 *  Protected Methods
	 *  ----------------------------------------------------------------------
	 */

	/**
	 * Get the non-abbreviated form of gender.
	 * 
	 * @return string
	 */
	protected function getGender()
	{
		if ( ! array_key_exists($this->gender, self::$genders))
		{
			return self::NOT_AVAILABLE;
		}

		return self::$genders[$this->gender];
	}

	/**
	 * Get the non-abbreviated form of eye color.
	 * 
	 * @return string
	 */
	protected function getEyeColor()
	{
		if ( ! array_key_exists($this->eyeColor, self::$abbreviations))
		{
			return self::NOT_AVAILABLE;
		}

		return self::$abbreviations[$this->eyeColor];
	}

	/**
	 * Search a string for the specified regex. Return first
	 * captured parenthesized subpattern if one was found.
	 *
	 * @param  string $data
	 * @param  string $regex
	 * @return string
	 */
	protected function find($data, $regex)
	{
		preg_match($regex, $data, $matches);

		return isset($matches[1]) ? $matches[1] : 'N/A';
	}

}