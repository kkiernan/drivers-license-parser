<?php

namespace Kiernan\Formats;

class AAMVA2012 extends AAMVA
{
	const FIRST_NAME = '/DAC(\S*)/';
	const MIDDLE_NAMES = '/DAD(\S*)/';

	protected $firstName;
	protected $middleNames;

	/**
	 * Create a new instance of the class. Add any additional properties
	 * to this implementation that do not exist in the abstract class.
	 * 
	 * @param string $data
	 */
	public function __construct($data)
	{
		parent::__construct($data);

		$this->firstName = $this->find($data, self::FIRST_NAME);
		$this->middleNames = $this->find($data, self::MIDDLE_NAMES);
	}
}