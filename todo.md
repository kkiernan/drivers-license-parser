# TODO

[X] Make sure all formats implement some kind of license interface.
[ ] Build out the license interface a bit more to ensure some kind of clean api?
[ ] Make sure static codes and abbreviations are in fact standard accross all versions of the AAMVA specification.
[ ] Implement weight range getter in AAMVA abstract class.
[ ] Way to simplify or abstract getters with format like eye color, weight range, etc?