<?php

use Kiernan\Formats\AAMVA2005;
use Kiernan\LicenseFactory;

require '../vendor/autoload.php';

$data = "@ - ANSI 636000030001DL00310440DLDCANONE DCB158X9 DCDS
DBA08142017 DCSMAURY DCTJUSTIN,WILLIAM
DBD08142009 DBB07151958 DBC1 DAYBRO DAU075 in DAG17 FIRST STREET
DAISTAUNTON DAJVA DAK244010000 DAQT16700185
DCF061234567 DCGUSA DCHS DDC00000000 DDB12102008 DDDN
DDAN DCK9060600000017843";

// If you knew the specific implemention, this would be fine...
$license = new AAMVA2005($data);

$eyeColor = $license->get('eyeColor'); // brown

// But you likely do not, so this factory helps you out...
$license = LicenseFactory::create($data);

print_r($license);

